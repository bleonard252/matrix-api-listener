import { MatrixClient, AutojoinRoomsMixin, SimpleFsStorageProvider } from 'matrix-bot-sdk';
async function f(req, res) {
    var client = new MatrixClient(
        'https://kde.modular.im',
        process.env.ACCESS_TOKEN,
        //new SimpleFsStorageProvider("store.json")
    );
    await AutojoinRoomsMixin.setupOnClient(client);
    var timeline = await client.getEventContext(req.query.channelid, req.query.event, 100);
    var room = await client.getRoomState(req.query.channelid);
    if (room == null) {
        res.status(404);
        res.send('Room does not exist or bot is not added');
    } else {
        var result = [];
        var edited = [];
        for (var i: number = timeline.after.length - 1; i >= 0; i--) {
            var message = timeline.after[i];
            if (message.type == "m.room.message" 
            && !message.unsigned["redacted_by"]
            && !(message.content["m.new_content"] && edited.includes(message.content["m.relates_to"]["event_id"]))
            && !(message.content["body"] || "").startsWith("/")) {
                if (message.content["m.new_content"]) {
                    var otherMessage = (await client.getEvent(req.query.channelid, message.content["m.relates_to"]["event_id"]));
                    edited.push(message.content["m.relates_to"]["event_id"]);
                    if (!otherMessage.type as unknown == "m.redaction") result.push({
                        "content": message.content["m.new_content"],
                        "timestamp": otherMessage["origin_server_ts"],
                        "edited": true
                    });
                } else if (!edited.includes(message.eventId)) {
                    result.push({
                        "content": message.content,
                        "timestamp": message.timestamp
                    });
                }
            }
        }
        result.reverse();
        res.json(result);
    }
}
module.exports = f;